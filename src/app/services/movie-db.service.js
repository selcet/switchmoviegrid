var angular = require('angular');
var app = angular.module('app');
var apiKey = 'ebea8cfca72fdff8d2624ad7bbf78e4c';


var movieDBService = app.service('movieDBService', function ($http) {
  var connectionURL = 'http://api.themoviedb.org/3/movie/now_playing?api_key=' + apiKey;
  var imagesPaths = 'http://image.tmdb.org/t/p/w342/';
  var defaultPosterPath = 'app/images/default-poster.jpg';

  function parseServerResponse(response) {
    var posters = response.results.map(function (result) {
      return {
        adult             : result.adult,
        backdrop_path     : result.backdrop_path,
        genre_ids         : result.genre_ids,
        id                : result.id,
        original_language : result.original_language,
        original_title    : result.original_title,
        overview          : result.overview,
        popularity        : result.popularity,
        poster_path       : (result.poster_path !== null) ? (imagesPaths + result.poster_path) : null,
        // poster_path       : (result.poster_path !== null) ? (imagesPaths + result.poster_path) : defaultPosterPath,
        title             : result.title,
        video             : result.video,
        vote_average      : result.vote_average,
        vote_count        : result.vote_count,
      }
    });

    return {
      page: response.page,
      total_pages: response.total_pages,
      total_results: response.total_results,
      posters: posters
    }
  }
  return {
    getMovieData: function(page) {
      return $http.get(connectionURL + '&page=' + (page || 1))
        .then(function (response) {
          return response;
        })
        .then(function (response) {
          return parseServerResponse(response.data);
        });
    }
  }
});
movieDBService.inject = ['$http'];

module.exports = movieDBService;
