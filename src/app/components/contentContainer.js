var template  = require('./contentContainer.html');
var styles    = require('./contentContainer.scss');

module.exports  = {
  template    : template,
  styles      : styles,
  controller  : ['movieDBService', function (movieDBService) {
    var self = this;
    self.postersInfo = {};

    self.onPageChange = function (pageNumber) {
      movieDBService.getMovieData(pageNumber)
        .then(function (info) {
          self.postersInfo = info;
        });
      //тут запрашивать нововыбранные постеры
    };

    movieDBService.getMovieData()
      .then(function (info) {
        self.postersInfo = info;

        // var i,j,chunk = 12;
        // var tempArray = [];
        // for (i = 0,j = self.postersInfo.posters.length; i < j; i += chunk) {
        //   tempArray = array.slice(i, i + chunk);
        //   self.postersInfo.posters = tempArray;
        // }
        //
        // $log('!!!' + tempArray);
      });
  }]
};
