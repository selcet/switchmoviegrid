var angular = require('angular');
var template = require('./slider.component.html');
var styles    = require('./slider.component.scss');

var app = angular.module('app');
var slider = app.component('slider', {
  template  : template,
  styles    : styles,
  bindings  : {
    pages   : '<',
    page    : '<',
    change  : '<'
  },

  controller: function () {  }
});


module.exports = slider;
