var angular = require('angular');
var template  = require('./grid.component.html');
var styles    = require('./grid.component.scss');

var app = angular.module('app');
var grid = app.component('grid', {
  template  : template,
  styles    : styles,
  bindings  : {
    posters: '<'
  },

  controller: function () {  }
});

module.exports = grid;
