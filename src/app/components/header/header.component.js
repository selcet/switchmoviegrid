var angular   = require('angular');
var template  = require('./header.component.html');
var styles    = require('./header.component.scss');

var app = angular.module('app');
var header = app.component('header', {
  template  : template,
  styles    : styles,
  bindings  : {
    hero: '='
  },

  controller: function () {  }
});


module.exports = header;
