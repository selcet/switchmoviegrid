var angular = require('angular');
var app = 'app';
angular
  .module(app, ['ui.router']);

var contentContainer = require('./app/components/contentContainer');
var angular = require('angular');
var header = require('./app/components/header/header.component');
var slider = require('./app/components/slider/slider.component');
var grid = require('./app/components/grid/grid.component');
var movieDBService = require('./app/services/movie-db.service');


require('angular-ui-router');
var routesConfig = require('./routes');

require('./index.scss');

angular
  .module(app)
  .config(routesConfig)
  .component('app', contentContainer);

module.exports = app;
