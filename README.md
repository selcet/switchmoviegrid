# [Switch Movie Grid - test task](https://bitbucket.org/selcet/switchmoviegrid)
### Switch Movie Grid - to view movies in grid view

## Environment Tools
- NodeJS (for downloading visit page - https://nodejs.org/en/download/)
- Gulp and Gulp modules. *Just run command from terminal*: `npm i`

## Running page
Check tasks with command:
- `gulp --tasks`

To run project for common needs:
- `gulp serve`
